# LLenguatges de Programació 
## Quarta Generació
### Definició de Quarta generació

La quarta generació es defineix com un lleguatge amb una llargada de 16 a 27 lineas de codi per punt implementat en una funció de quarta generació. 


És un entorn dels paquets de software de desenvolupament de sistemas, incloent els llenguatges de programació d'alt nivell.

![foto](https://paquetesinformaticoscuc1330241.files.wordpress.com/2016/10/desarrollo_software.jpg?w=630)

### Que es?

Son eines prefabricadas, que donen lloc a un llenguatge de programació d'alt nivell, s'assembla mes al ángles que a un llenguatge de tercera generació, perque s'allunya mes de ser un procediment. Poden accedir a bases de dades.

![foto](http://www.webscostadelsol.com/wp-content/uploads/2014/02/herramientas-programacion-web.png)
 


### Avantatges

* Permeten elaborar programes mes rápid amb lo qual augmenta la productivitat.

* Els programadors tenen menys esgotament, ja que generalment requereix escriure menys.

* El nivel de concetració es menos,ja que algunes instruccions les fan les eines.

* Quan hi ha que editar i/o arreglar programas elaborats es menys complicat,

![foto](https://st2.depositphotos.com/1431107/12433/v/950/depositphotos_124334428-stock-illustration-green-tick-check-mark-icon.jpg)

### Desventatges

* Les eines son menys flexibles que els llenguatges d'alt nivell.

* Es crea dependencia de les eines, es perd autonomia. Ja que les eines contenen llibrerias de diferents proveïdors, i fa que s'hagin d'instalar add-ons. Generalment només executen amb l'eina que els va crear, llevat que existin acords entre els proveïdors.

* Normalment no compleixen els estándards internacionals. Per aquest motiu es molt arriesgat de cara a un futur, perqué no se sap quant temps estará l'eina i el seu fabricant al mercat.

![foto](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRbYRNfkGG6X3u4pF1c1Sb_Hhg-CiK6sYANE_lkQS2cEg-4S1wV)

### Exemple (Phyton)
Aquest programa imprimiría els numeros impars del 1 al 25.

```pyhton
n = 1
h = ''
while n <= 25:
    if n%2 != 0:
        h += ' %i' % n
    n += 1
print h
```
# Generacions dels llenguatges de programació

[Cinquena-Generació](https://gitlab.com/JaumeBori/generacion-2-/blob/master/5gen.md)
