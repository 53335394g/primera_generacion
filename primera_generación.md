# LLenguatges de Programació 

## Primera Generació

### Origen
* **Quan?** La primera generació de llenguatges, es va iniciar a finals del 1940.

* **Qui?** Tot va començar amb el concepte de programa emmagatzemat de Von Neumann. Aquest concepte va significar un gran pas de la programació per ordinadors.





![foto](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/JohnvonNeumann-LosAlamos.gif/220px-JohnvonNeumann-LosAlamos.gif)

El procediment era díficil i complicat.

### Utilització

* A principis del 1950, el programador va a ver d'utilitzar el llenguatge de les máquines. 

![foto](http://s3.amazonaws.com/s3.timetoast.com/public/uploads/photos/7748422/niko0007.jpg?1478148919)

* L'únic llenguatge que el hardwdare acceptaba i era capaç d'entendre era el binari.

![foto](https://k45.kn3.net/taringa/2/4/2/9/7/1/61/nejihyuga1546/D21.jpg?538)

* Aquest llenguatge dificultaba la escritura de programas.

![foto](http://www.nerdilandia.com/wp-content/uploads/2014/08/sublime-text.jpg)

* Per aquesta complexitat, la modificació i/o correcció d'errors era molt complicada.

* Només es podia utilitzar en un tipus de máquina ja que cada máquina tenia el seu llenguatge.

![foto](http://2.bp.blogspot.com/-xwTNXOd166I/VbV7Vph763I/AAAAAAAAAAg/o-8QUvg23zA/s1600/maxresdefault.jpg)

* El llenguatge tenia l'avantatge de explorar al máxim les posibilitats llógiques i la capacitat física del equip. 

![foto](https://s3.amazonaws.com/s3.timetoast.com/public/uploads/photos/9589603/Zuse_Z4.jpg?1487217378)

# Generacions dels llenguatges de programació

[Segona_Generació](https://gitlab.com/JaumeBori/generacion-2-/blob/master/SegonaGeneracio.md)

[Tercera-Generació](https://gitlab.com/JaviEV/tercera_generacion/blob/master/tercera_generacion.md)

[Quarta_Generació](https://gitlab.com/53335394g/primera_generacion/blob/master/cuarta_generaci%C3%B3n.md)

[Cinquena-Generació](https://gitlab.com/JaumeBori/generacion-2-/blob/master/5gen.md)
